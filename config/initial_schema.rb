def create_schema
  ActiveRecord::Schema.define do

    create_table :messages, force: true do |t|
      t.string :header, limit: 8192
      t.text :body
      t.belongs_to :mailbox
    end

    create_table :mailboxes, force: true do |t|
      t.string :name
      t.belongs_to :user
      t.integer :parent_id
    end

    create_table :email_addresses, force: true do |t|
      t.string :name
      t.belongs_to :contact_record
    end

    create_table :contact_records, force: true do |t|
      t.belongs_to :user
      t.string :full_name
      t.string :company
      t.references :home_address
      t.references :business_address
    end

    create_table :users, force: true do |t|
      t.string :username
      t.string :password
      t.belongs_to :domain
      t.string :primary_alias
      # t.references :contact_record
    end

    create_table :domains, force: true do |t|
      t.string :name, null: false
    end

    create_table :mailbox_hierarchies, force: true do |t|
      t.integer :ancestor_id
      t.integer :descendant_id
      t.integer :generations
    end

    create_table :street_addresses, force: true do |t|
      t.string :street
      t.string :city
      t.string :country
      t.string :zip
    end

    # delete all users belonging to deleted domain
    add_foreign_key :users, :domains, on_delete: :cascade
    # on used deletion delete all his mailboxes
    add_foreign_key :mailboxes, :users, on_delete: :cascade
    # with mailbox delete all its messages too
    add_foreign_key :messages, :mailboxes, on_delete: :cascade
    # delete sub-folders of deleted folder
    add_foreign_key :mailboxes, :mailboxes, column: :parent_id, on_delete: :cascade
    # delete contact details of deleted user
    add_foreign_key :contact_records, :users, on_delete: :cascade
    add_foreign_key :contact_records, :street_addresses, column: :home_address_id, on_delete: :cascade
    add_foreign_key :contact_records, :street_addresses, column: :business_address_id, on_delete: :cascade
    # with deleted contact record remove its e-mail addresses too
    add_foreign_key :email_addresses, :contact_records, on_delete: :cascade
  end
end