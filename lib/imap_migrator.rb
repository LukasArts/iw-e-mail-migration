require 'closure_tree'
require 'pathname'

require_relative 'models/street_address'
require_relative 'models/contact_record'
require_relative 'models/domain'
require_relative 'models/email_address'
require_relative 'models/user'
require_relative 'models/message'
require_relative 'models/mailbox'

class ImapMigrator

  def migrate(root_path:)
    Log.log.debug { 'root_path: ' + "\"#{root_path}\"" }
    validate_path root_path
    @root_path.each_child do |domain_path|
      process_domain domain_path if domain_path.directory?
    end
  end

  def validate_path(root_path)
    begin
      raise ArgumentError, "#{root_path} is not a directory" unless FileTest.directory? root_path
    rescue ArgumentError => e
      Log.log.error { e.message }
      exit 2
    end
    @root_path = Pathname.new(root_path).cleanpath
  end

  def process_domain(domain_path)
    @domain = Domain.find_or_create_by(name: domain_path.basename.to_s)
    domain_path.each_child do |user_path|
      process_user user_path if user_path.directory?
    end
  end

  def process_user(user_path)
    Log.log.debug { " in a process_user(user_path: ' + #{user_path})" }
    @user = User.find_or_create_by(username: user_path.basename.to_s, domain: @domain)
    @user_path = user_path
    associate_contact_details @user
    user_path.each_child do |mailbox_path|
      process_mailbox mailbox_path if mailbox_path.directory?
    end
  end

  def associate_contact_details(user)
    user.contact_record = ContactRecord.create()
  end

  def process_mailbox(mailbox_path)
    # perform DFS on given mailbox, looking for e-mails and subfolders
    Log.log.debug { "  in a process_mailbox(mailbox_path: #{mailbox_path})" }
    # create top level mailbox
    @user.mailboxes.find_or_create_by(name: mailbox_path.relative_path_from(@user_path).to_s)
    mailbox_path.find do |f|
      if f.directory?
        process_folder(f)
        next
      elsif f.fnmatch('*.imap', File::FNM_DOTMATCH)
        process_message f
        next
      else
        Log.log.info { "  ! skipping unexpected file #{f}" }
      end
    end
  end

  def process_folder(folder)
    Log.log.debug { "   in a process_folder(folder: #{folder})" }
    Log.log.debug { "    - relative path: #{folder.relative_path_from @user_path}" }
    @user.mailboxes.find_or_create_by_path((folder.relative_path_from @user_path).to_s.split(File::SEPARATOR))
  end

  def process_message(filename)
    Log.log.debug { "     -> in a process_message(filename: #{filename}" }
    header, body = '', ''
    target = header
    File.foreach(filename) do |line|
      target = body if line.chomp == ''
      target << line
    end

    current_mailbox = @user.mailboxes.find_or_create_by_path((filename.dirname.relative_path_from @user_path).to_s.split(File::SEPARATOR))
    current_mailbox.messages.create(header: header, body: body)
    current_mailbox.save
  end

end