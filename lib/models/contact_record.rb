class ContactRecord < ActiveRecord::Base
  belongs_to :home_address, class_name: 'StreetAddress'
  belongs_to :business_address, class_name: 'StreetAddress'
  has_many :email_addresses
  belongs_to :user
end