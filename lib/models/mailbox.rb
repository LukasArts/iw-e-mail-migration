class Mailbox < ActiveRecord::Base
  belongs_to :user
  has_many :messages

  acts_as_tree

end