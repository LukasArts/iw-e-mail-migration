class User < ActiveRecord::Base
  has_many :mailboxes
  has_many :messages, through: :mailboxes
  belongs_to :domain
  has_one :contact_record
  has_many :street_addresses, inverse_of: :contact_records

  def to_s
    "#{id} - #{username}@#{domain.name}"
  end

end
