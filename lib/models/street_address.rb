class StreetAddress < ActiveRecord::Base
  has_one :contact_record, inverse_of: :home_address
  has_one :contact_record, inverse_of: :business_address
end