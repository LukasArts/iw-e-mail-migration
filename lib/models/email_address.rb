class EmailAddress < ActiveRecord::Base
  belongs_to :contact_item, :inverse_of => :email_addresses
end