class CLI
  def self.cli_flags
    options = Slop::Options.new
    options.banner = 'Usage: migrate <root_path> [options]'
    # options.separator ''
    options.separator 'Options:'
    options.bool '-v', '--verbose', 'enable verbose mode'
    options.string '--db-config', 'configuration name from database.yml (default: development)', default: 'development'
    options.string '--dump-schema', 'file where to dump current db schema', default: 'db_schema.rb'
    options.integer '-ll', '--log-level', '0-5 (lower level increases output verbosity, default: 3)', default: 3
    options.boolean '--queries', 'show SQL queries'
    options.boolean '--initialize-db', ' recreate database schema from “config/initial_schema.rb”'
    options
  end

  def self.options(command_line_options = ARGV)
    parser = Slop::Parser.new cli_flags
    self.parse_arguments(command_line_options, parser)
  end

  def self.parse_arguments(command_line_options, parser)
    begin
      result = parser.parse command_line_options

    rescue Slop::Error => e
      puts e.message
      puts cli_flags
      exit 3
    end

    # there should be exactly one argument unprocessed by Slop
    unless result.arguments.size == 1
      puts 'path to root storage missing'
      puts cli_flags
      exit 1
    end

    unless Dir.exist? result.arguments.first
      puts "\"#{result.arguments.first}\" is not a valid path to storage root"
      exit 2
    end

    result
  end
end
