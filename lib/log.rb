class Log
  def self.log
    if @logger.nil?
      @logger = Logger.new STDERR
      @logger.level = Logger::ERROR
      @logger.datetime_format = '%Y-%m-%d %H:%M:%S '
    end
    @logger
  end

end