Iᴍᴀᴘ2db
=======
This application processes e-mail messages stored in a directory tree and saves them in an arbitrary SQL database.

    Usage: migrate <root_path> [options]
     Options:
         -v, --verbose     enable verbose mode
         --db-config       configuration name from database.yml (default: development)
         --dump-schema     file where to dump current db schema
         -ll, --log-level  0-5 (lower level increases output verbosity, default: 3)
         --queries         show SQL queries
         --initialize-db   recreate database schema from “config/initial_schema.rb”


## Motivation
Our initial assumption ([though not universally supported](http://www.memoryhole.net/~kyle/databaseemail.html)) is that there are benefits in using database backend for e-mail server. We therefore need to convert an original user base from a file based backend to a relational database.

The application expects e-mails to be stored in a hierarchy of directories following this schema:

    domain.com/user/mailbox/[folder/subfolder…]message.imap

where:

- a `mailbox` and `/subfolder/sub-subfolder…` hierarchy is perceived as a mailbox naming hierarchy like referred to in [RFC 3501](https://tools.ietf.org/html/rfc3501),
- a message is a file and its content is expected to follow format described [RFC 5322](https://tools.ietf.org/html/rfc5322) (a message header is separated from the message body with a blank line).

## Installation
Issue `bundler install` command with [bundler](http://bundler.io) installed.

### Configuration
Enclosed `Rakefile.rb` offers some database management tasks through `rake` command. Issue `rake --tasks` to see the list of tasks available.

## Examples
Create all databases specified in `db/database.yml`:

    $ rake -v db:create:all
    
Produce `db/schema.rb` containing information about production database. 
    
    $ ENV=production rake db:dump

Migrate messages from **root** folder into _development_ database initializing database schema with configuration from `db/initial_schema.rb` displaying executed SQL queries.

    $ bin/migrate root --initialize-db --queries --db-config development