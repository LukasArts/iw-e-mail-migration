# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 0) do

  create_table "contact_records", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "user_id"
    t.string "full_name"
    t.string "company"
    t.integer "home_address_id"
    t.integer "business_address_id"
    t.index ["business_address_id"], name: "index_contact_records_on_business_address_id", using: :btree
    t.index ["home_address_id"], name: "index_contact_records_on_home_address_id", using: :btree
    t.index ["user_id"], name: "index_contact_records_on_user_id", using: :btree
  end

  create_table "domains", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name", null: false
  end

  create_table "email_addresses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.integer "contact_record_id"
    t.index ["contact_record_id"], name: "index_email_addresses_on_contact_record_id", using: :btree
  end

  create_table "mailbox_hierarchies", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "ancestor_id"
    t.integer "descendant_id"
    t.integer "generations"
  end

  create_table "mailboxes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.integer "user_id"
    t.integer "parent_id"
    t.index ["parent_id"], name: "fk_rails_e4cabd9d6f", using: :btree
    t.index ["user_id"], name: "index_mailboxes_on_user_id", using: :btree
  end

  create_table "messages", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "header", limit: 8192
    t.text "body", limit: 65535
    t.integer "mailbox_id"
    t.index ["mailbox_id"], name: "index_messages_on_mailbox_id", using: :btree
  end

  create_table "street_addresses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "street"
    t.string "city"
    t.string "country"
    t.string "zip"
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "username"
    t.string "password"
    t.integer "domain_id"
    t.string "primary_alias"
    t.index ["domain_id"], name: "index_users_on_domain_id", using: :btree
  end

  add_foreign_key "contact_records", "street_addresses", column: "business_address_id", on_delete: :cascade
  add_foreign_key "contact_records", "street_addresses", column: "home_address_id", on_delete: :cascade
  add_foreign_key "contact_records", "users", on_delete: :cascade
  add_foreign_key "email_addresses", "contact_records", on_delete: :cascade
  add_foreign_key "mailboxes", "mailboxes", column: "parent_id", on_delete: :cascade
  add_foreign_key "mailboxes", "users", on_delete: :cascade
  add_foreign_key "messages", "mailboxes", on_delete: :cascade
  add_foreign_key "users", "domains", on_delete: :cascade
end
